import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

df = pd.read_csv("SYN_ACK.csv")

## Columns Are as follows          frame.number     frame.len      ip.src       ip.dst      tcp.srcport     tcp.dstport   ##

#print(df.head())
#print(df.info())

## Mapping ##

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

df = pd.read_csv('SYN_ACK.csv')

df_sample = df.sample(frac=0.01)

plt.figure(figsize=(12, 8))
sns.heatmap(df_sample.pivot_table(index='ip.src', columns='tcp.dstport', values='tcp.srcport', aggfunc='count'), cmap='Blues', annot=True, fmt='g')
plt.xlabel('TCP Destination Port')
plt.ylabel('IP Source')
plt.title('IP & TCP Destination Port')
plt.show()



